
const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://admin123:admin123@cluster0.yl6wic9.mongodb.net/s35?retryWrites=true&w=majority", {
   useNewUrlParser: true,
   useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Connected to MongoDb Atlas"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// 1. User Schema

const userSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// 2. User Model
const Signup = mongoose.model("Signup", userSchema);

// 3. Post route

app.post("/signup", (req, res) => {

	Signup.findOne({name: req.body.name}, (err, result) => {
		if (result !== null && result.name == req.body.name){
			return res.send("User already exist!")
		}
		else
		{
			let newUser = new Signup({
				name: req.body.name
			});

			newUser.save((saveErr, savedTask) => {
				if (saveErr){
				return console.error(saveErr)
				}
				else
				{
					return res.status(201).send("New user registered!");
				};
			})
		};
	});
});






app.listen(port, () => console.log(`Server is running at port: ${port}`));