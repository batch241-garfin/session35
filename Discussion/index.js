
// DATA PERSISTENCE via MONGOOSE ODM

// ODM - Object Document Mapper
// Mongoose - an ODM library
// Schemas - representation of document structure.
// Models - A programming interface for querying and manipulating a database. Provide functionalities.


const express = require("express");

// Allows creation of Schemas to model our data structures
// Also has access to a number of methods for manipulation our database
const mongoose = require("mongoose");
const app = express();
const port = 3000;

// MongoDB Connection

// Syntax:
// mongoose.connect("<MongoDB connection string>, {useNewUrlParser: true}")

// Connecting to MongoDB atlas
// Add password and database name

mongoose.connect("mongodb+srv://admin123:admin123@cluster0.yl6wic9.mongodb.net/s35?retryWrites=true&w=majority", {

	// Due to update in MongoDb drivers that allow connection to it, the default connection is being flagged as error.
	// Allows us to avoid any current and fututre errors while connecting to MongoDB
   useNewUrlParser: true,
   useUnifiedTopology: true
});

// Connecting to MongoDB locally

let db = mongoose.connection;

// if a connection error occured, print the output in terminal
db.on("error", console.error.bind(console, "connection error"));

// if the connection is successful, print the message in the terminal
db.once("open", () => console.log("Connected to MongoDb Atlas"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Mongoose Schema

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending" // or required: "status is required"
	}
});


// Models

// Models must be singular form and capitalized
// first parameter - indicates the collection where to store data
// second parameter - specify the schema/blueprint of the documents that will be stored in the MongoDb collection.
const Task = mongoose.model("Task", taskSchema);

// Creating a New Task

//Business Logic

//  Add a functionality to check if there are duplicate tasks
// - If the task already exists in the database, we return an error
// - If the task doesn't exist in the database, we add it in the database
// The task data will be coming from the request's body
// Create a new Task object with a "name" field/property
// The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object

app.post("/task", (req, res) => {

	// To check if there are duplicate tasks
	Task.findOne({name: req.body.name}, (err, result) => {
		if (result !== null && result.name == req.body.name){
			return res.send("Duplicate task found!")
		}
		else
		{
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, savedTask) => {
				if (saveErr){
				return console.error(saveErr)
				}
				else
				{
					return res.status(201).send("New task created");
				};
			})
		};
	});
});

// Get all the tasks

// Business Logic
// Retrieve all the documents
// If an error is encountered, print the error
// If no errors are found, send a success status back to the client/Postman and return an array of documents


app.get("/tasks", (req, res) => {
	Task.find({},(err,result) => {
		if (err){
			return console.log(err);
		}
		else
		{
			return res.status(200).json({
				data: result
			});
		};
	});
});









app.listen(port, () => console.log(`Server is running at port: ${port}`));
